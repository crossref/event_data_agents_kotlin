FROM openjdk:11.0.10-jre-buster

COPY target/agents-*.jar ./agents.jar

ENTRYPOINT ["java", "-jar", "agents.jar"]