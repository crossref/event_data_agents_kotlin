import com.amazonaws.services.s3.model.PutObjectResult
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.util.StdDateFormat
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.readValue
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.fuel.core.Response
import com.github.kittinunf.fuel.core.ResponseResultOf
import com.github.kittinunf.fuel.core.extensions.httpString
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import io.mockk.*
import io.mockk.junit5.MockKExtension
import kotlinx.coroutines.*
import org.crossref.agents.common.JWTSigner
import org.crossref.agents.common.Resources
import org.crossref.agents.integrations.AwsS3
import org.crossref.agents.integrations.KAFKA_MESSAGE_MAX_BYTES
import org.crossref.agents.integrations.Kafka
import org.crossref.agents.metadata.*
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.fail
import org.springframework.core.io.ClassPathResource
import java.nio.file.Files
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.util.*
import java.util.concurrent.Executors
import kotlin.system.measureTimeMillis

@ExtendWith(MockKExtension::class)
class MetadataAgentTest {

    private val s3 = mockk<AwsS3>()
    private val kafka = mockk<Kafka>()
    private val resources = mockk<Resources>()
    private val jwtSigner = JWTSigner("TEST")
    private val metadataAgent =
        MetadataAgent(
            "https://api.crossref.org/v1",
            "",
            "",
            "1000",
            10,
            "metadata-agent@crossref.org",
            10,
            4,
            4,
            s3, kafka, resources, jwtSigner)

    private fun LocalDate.toCheckpointString() = """{"last-day-processed":"$this"}"""

    private val jsonMapper = ObjectMapper().apply {
        registerKotlinModule()
        registerModule(JavaTimeModule())
        disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
        dateFormat = StdDateFormat()
    }

    private fun jsonFileToPage(path: String): Input.Page =
        jsonMapper.readValue(String(Files.readAllBytes(ClassPathResource(path).file.toPath())))

    private val observation = Output.Observation(
        "type",
        """Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                |sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                |Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
                |nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
                |reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
                |Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia 
                |deserunt mollit anim id est laborum.""".trimMargin())

    @Test
    fun testRunWithNotOldEnoughCheckpoint() {

        val agent = spyk(metadataAgent)
        val pastDate = LocalDate.now().minusDays(1)

        every { s3.fileExists(any(), any()) } returns true
        every { s3.get(any(), any()) } returns pastDate.toCheckpointString()
        excludeRecords {
            agent.s3
            agent.run()
            agent.getExistingCheckpoint(any())
        }

        agent.run()

        verify(exactly = 1) { agent.processFromCheckpoint(false, null) }
        verify(exactly = 0) { agent.process(any(), any()) }

        confirmVerified(agent)

    }

    @Test
    fun testRunWithOldEnoughCheckpoint() {

        val agent = spyk(metadataAgent)
        val pastDate = LocalDate.now().minusDays(2)

        every { s3.fileExists(any(), any()) } returns true
        every { s3.get(any(), any()) } returns pastDate.toCheckpointString()
        justRun { agent.process(usePolitePool = false, saveCheckpoint = true, fromDateInclusive = any()) }
        excludeRecords {
            agent.s3
            agent.run()
            agent.getExistingCheckpoint(any())
        }

        agent.run()

        verify(exactly = 1) { agent.processFromCheckpoint(false, saveCheckpoint = null) }
        verify(exactly = 1) {
            agent.process(
                usePolitePool = false,
                saveCheckpoint = true,
                fromDateInclusive = pastDate.plusDays(1)
            )
        }

        confirmVerified(agent)

    }

    @Test
    fun testRunWithoutCheckpoint() {

        val agent = spyk(metadataAgent)

        every { s3.fileExists(any(), any()) } returns false
        justRun { agent.process(any(), any()) }
        excludeRecords {
            agent.s3
            agent.run()
            agent.getExistingCheckpoint(any())
        }

        agent.run()

        verify(exactly = 1) { agent.processFromCheckpoint(false, null) }
        verify(exactly = 0) {
            agent.process(
                usePolitePool = any(),
                saveCheckpoint = any(),
                fromDateInclusive = any()
            )
        }

        confirmVerified(agent)

    }

    @Test
    fun testRunWitErroneousCheckpoint() {

        val agent = spyk(metadataAgent)

        every { s3.fileExists(any(), any()) } returns true
        every { s3.get(any(), any()) } returns "@#$#"

        try {
            agent.run()
            fail("Exception not thrown despite malformed checkpoint")
        } catch (e: Exception) {
            println("Exception correctly thrown with message: ${e.message}")
        }
    }

    @Test
    fun testRunWitFutureCheckpoint() {

        val agent = spyk(metadataAgent)
        val pastDate = LocalDate.now().plusDays(2)

        every { s3.fileExists(any(), any()) } returns true
        every { s3.get(any(), any()) } returns pastDate.toCheckpointString()
        justRun { agent.process(false, any()) }
        excludeRecords {
            agent.s3
            agent.run()
            agent.getExistingCheckpoint(any())
        }

        agent.run()

        verify(exactly = 1) { agent.processFromCheckpoint(false, null) }
        verify(exactly = 0) {
            agent.process(
                usePolitePool = false,
                saveCheckpoint = true,
                fromDateInclusive = pastDate.plusDays(1)
            )
        }

        confirmVerified(agent)
    }

    @Test
    fun testFromWorkToEvidenceRecord() {
        val agent = spyk(metadataAgent)
        val requestValues = mutableListOf<String>()

        val pages: List<Input.Page> = listOf(jsonMapper.readValue(REST_API_RESPONSE))
        every { s3.put(any(), any(), any()) } returns PutObjectResult()
        every { resources.getVersion() } returns "0.1.0-SNAPSHOT"
        justRun { kafka.send(any(), any(), capture(requestValues)) }
        every { agent.collectDataForDates(false, any(), any()) } returns pages.asSequence()
        every { agent.timeStamp() } returns LocalDateTime.of(LocalDate.EPOCH, LocalTime.MIDNIGHT)
        every { agent.uniqueEvidenceRecordId(any()) } returns "20210328-metadata-92fbfc55-48a4-4150-a74a-d80eaed092ce"

        val args = arrayOf(
            "-s", "false",
            "-f", LocalDate.now().minusDays(2).toString(),
            "-u", LocalDate.now().toString()
        )
        agent.run(args)

        Assertions.assertEquals(listOf(EVIDENCE_RECORD_1_1, EVIDENCE_RECORD_1_1), requestValues)
    }

    @Test
    fun testDaemonFlag() {
        val agent = spyk(metadataAgent)
        val args = arrayOf("-d")

        every { agent.processFromCheckpoint(usePolitePool = false, saveCheckpoint = true) } returns false
        excludeRecords {
            agent.run(args)
        }

        agent.run(args)

        verify(exactly = 0) { agent.processPeriod(true, any(), null) }
        verify(exactly = 1) { agent.processFromCheckpoint(false, saveCheckpoint = true) }
        confirmVerified(agent)
    }

    @Test
    fun testFunctionFlags() {
        val agent = spyk(metadataAgent)
        val args = arrayOf("-d", "-r")

        every { agent.processFromCheckpoint(usePolitePool = false, saveCheckpoint = true) } returns false
        excludeRecords {
            agent.run(args)
        }

        agent.run(args)
        verify(exactly = 1) { agent.processFromCheckpoint(false, saveCheckpoint = true) }
        confirmVerified(agent)
    }

    @Test
    fun testDaemonFlagWithFromDate() {
        val agent = spyk(metadataAgent)
        val args = arrayOf("-d", "-f", "2020-04-12")

        justRun { agent.processPeriod(false, saveCheckpoint = true, fromDateInclusive = any()) }
        every { agent.processFromCheckpoint(false, saveCheckpoint = true) } returns false
        excludeRecords {
            agent.run(args)
        }

        agent.run(args)

        verify(exactly = 0) {
            agent.processPeriod(
                usePolitePool = any(),
                saveCheckpoint = any(),
                fromDateInclusive = any(),
                untilDateExclusive = any()
            )
        }
        verify(exactly = 1) { agent.processFromCheckpoint(false, saveCheckpoint = true) }
        confirmVerified(agent)
    }

    @Test
    fun testSingleExecutionWithFromDate() {
        val agent = spyk(metadataAgent)
        val args = arrayOf("-s", "true", "-f", "2020-04-12")

        justRun {
            agent.processPeriod(
                usePolitePool = false,
                saveCheckpoint = true,
                fromDateInclusive = any(),
                untilDateExclusive = any()
            )
        }
        excludeRecords {
            agent.run(args)
        }

        agent.run(args)

        verify(exactly = 1) {
            agent.processPeriod(
                usePolitePool = false,
                saveCheckpoint = true,
                fromDateInclusive = any(),
                untilDateExclusive = null
            )
        }
        confirmVerified(agent)
    }

    @Test
    fun testSingleExecutionWithFromDateNoCheckpoint() {
        val agent = spyk(metadataAgent)
        val args = arrayOf("-f", "2020-04-12")

        justRun {
            agent.processPeriod(
                false,
                saveCheckpoint = false,
                fromDateInclusive = any(),
                untilDateExclusive = any()
            )
        }
        excludeRecords {
            agent.run(args)
        }

        agent.run(args)

        verify(exactly = 1) {
            agent.processPeriod(
                false,
                saveCheckpoint = false,
                fromDateInclusive = any(),
                untilDateExclusive = null
            )
        }
        confirmVerified(agent)
    }

    @Test
    fun testBuildRequest() {
        var request = metadataAgent.buildRequest(
            true,
            "has-relation:1",
            LocalDate.EPOCH,
            LocalDate.EPOCH,
            "AoJ55/fMsvcCPxBodHRwOi8vZHguZG9pLm9yZy8xMC4xMDkzL2Jpb2luZm9ybWF0aWNzL2J0eDgxMw=="
        )

        Assertions.assertEquals(
            "GET https://api.crossref.org/v1/works?filter=has-relation:1,from-update-date:1970-01-01,until-update-date:1970-01-01&rows=10&cursor=AoJ55/fMsvcCPxBodHRwOi8vZHguZG9pLm9yZy8xMC4xMDkzL2Jpb2luZm9ybWF0aWNzL2J0eDgxMw==&mailto=metadata-agent@crossref.org",
            request.httpString().trim()
        )

        request = metadataAgent.buildRequest(
            false,
            "has-relation:1",
            LocalDate.EPOCH,
            LocalDate.EPOCH,
            "AoJ55/fMsvcCPxBodHRwOi8vZHguZG9pLm9yZy8xMC4xMDkzL2Jpb2luZm9ybWF0aWNzL2J0eDgxMw=="
        )

        Assertions.assertEquals(
            "GET https://api.crossref.org/v1/works?filter=has-relation:1,from-update-date:1970-01-01,until-update-date:1970-01-01&rows=10&cursor=AoJ55/fMsvcCPxBodHRwOi8vZHguZG9pLm9yZy8xMC4xMDkzL2Jpb2luZm9ybWF0aWNzL2J0eDgxMw==",
            request.httpString().trim()
        )
    }

    @Test
    // collectForDates should keep retrieving pages until API returns empty page.
    fun testCollectForDatesShouldRespectCursor() {
        val agent = spyk(metadataAgent)

        val json1 = jsonFileToPage("query-response-1.json")
        val json2 = jsonFileToPage("query-response-2.json")
        val json3 = jsonFileToPage("query-response-3.json")

        val request = "https://crossref.org".httpGet(listOf("a" to "1", "b" to "2"))

        every { agent.totalResultsCount(any(), any(), any(), any()) } returns 30
        every { agent.progressString(any(), any()) } returns "progress..."
        every { agent.pageForUrl(any()) } returnsMany listOf(json1, json2, json3)
        every { agent.buildRequest(false, any(), any(), any(), any(), false) } returns request
        every { agent.allFilterCombinations(emptyList()) } returns listOf("has-relation:1")

        excludeRecords {
            agent.collectDataForDates(false, LocalDate.EPOCH, LocalDate.EPOCH)
            agent.progressString(any(), any())
        }

        // The forEach is needed to force the sequence to materialise as sequences are lazy.
        // Please ignore any warnings and don't remove it.
        agent.collectDataForDates(false, LocalDate.EPOCH, LocalDate.EPOCH).forEach { it }

        // mock out a sequence of 3-Page objects, last has < RESULTS
        // mock out pageForUrl function
        // verify it's called 3 times, with the right cursor values
        verify(exactly = 1) { agent.totalResultsCount(emptyList(), false, LocalDate.EPOCH, LocalDate.EPOCH) }
        verify(exactly = 1) {
            agent.buildRequest(
                false,
                "has-relation:1",
                LocalDate.EPOCH,
                LocalDate.EPOCH,
                "*",
                false
            )
        }
        verify(exactly = 1) {
            agent.buildRequest(
                false,
                "has-relation:1",
                LocalDate.EPOCH,
                LocalDate.EPOCH,
                json1.message.nextCursor!!
            )
        }
        verify(exactly = 1) {
            agent.buildRequest(
                false,
                "has-relation:1",
                LocalDate.EPOCH,
                LocalDate.EPOCH,
                json2.message.nextCursor!!,
                false
            )
        }
        verify(exactly = 3) { agent.pageForUrl(request) }
        verify(exactly = 1) { agent.allFilterCombinations(emptyList()) }

        confirmVerified(agent)
    }

    @Test
    fun testPageForUrlCanParseResult() {
        // mock out fuel response
        // test that json is parsed
        val request = mockk<Request>()
        val response = mockk<Response>()

        every { request.responseString() } returns ResponseResultOf(
            request,
            response,
            Result.success(REST_API_RESPONSE)
        )
        val resultWithType = metadataAgent.pageForUrl(request)
        Assertions.assertEquals("book-chapter", resultWithType.message.items[0].type)

        every { request.responseString() } returns ResponseResultOf(
            request,
            response,
            Result.success(REST_API_RESPONSE_NO_TYPE)
        )
        val resultWithoutType: Input.Page = metadataAgent.pageForUrl(request)
        Assertions.assertNull(resultWithoutType.message.items[0].type)
    }

    @Test
    fun testPageForUrlCannotGetResult() {
        val request = mockk<Request>()
        val response = mockk<Response>()
        val error = mockk<FuelError>()

        every { request.responseString() } returns ResponseResultOf(request, response, Result.error(error))
        try {
            metadataAgent.pageForUrl(request)
            fail("An exception should be thrown if we are unable to get a response")
        } catch (e: java.lang.Exception) {
            println("Exception correctly thrown with message: ${e.message}")
        }
    }

    /**
     * This test misses the Sentry call and allows it to happen as well which is not good.
     * The only thing preventing this from being a red flag is that the call does not reach sentry
     * as there are no credentials available during the unit tests. Ideally it should be improved.
     */
    @Test
    fun testEmptySequenceReturnedWhenTypeIsMissing() {
        val rsp = jsonMapper.readValue<Input.Page>(REST_API_RESPONSE_NO_TYPE)
        val actions = metadataAgent.toActions(rsp.message.items[0])
        Assertions.assertEquals(emptySequence<Output.Action>(), actions)
    }

    @Test
    fun testProcessPeriodWithUntilDate() {
        val agent = spyk(metadataAgent)

        justRun { agent.process(usePolitePool = false, false, LocalDate.EPOCH, LocalDate.EPOCH) }
        excludeRecords { agent.processPeriod(usePolitePool = false, false, LocalDate.EPOCH, LocalDate.EPOCH) }

        agent.processPeriod(usePolitePool = false, false, LocalDate.EPOCH, LocalDate.EPOCH)

        verify(exactly = 1) { agent.process(usePolitePool = false, false, LocalDate.EPOCH, LocalDate.EPOCH) }

        confirmVerified()
    }

    @Test
    fun testProcessPeriodWithoutUntilDate() {
        val agent = spyk(metadataAgent)

        justRun { agent.process(usePolitePool = false, saveCheckpoint = false, fromDateInclusive = LocalDate.EPOCH) }
        excludeRecords {
            agent.processPeriod(
                usePolitePool = false,
                saveCheckpoint = false,
                fromDateInclusive = LocalDate.EPOCH
            )
        }

        agent.processPeriod(usePolitePool = false, saveCheckpoint = false, fromDateInclusive = LocalDate.EPOCH)

        verify(exactly = 1) {
            agent.process(
                usePolitePool = false,
                saveCheckpoint = false,
                fromDateInclusive = LocalDate.EPOCH
            )
        }

        confirmVerified()
    }

    @Test
    fun testSaveCheckpoint() {
        val agent = spyk(metadataAgent)

        every { s3.put(any(), any(), any()) } returns PutObjectResult()
        justRun { kafka.send(any(), any(), any()) }
        every { agent.collectDataForDates(false, any(), any()) } returns emptySequence()
        excludeRecords {
            agent.process(
                usePolitePool = false,
                saveCheckpoint = true,
                fromDateInclusive = LocalDate.EPOCH.minusDays(2),
                untilDateExclusive = LocalDate.EPOCH
            )
        }

        agent.process(
            usePolitePool = false,
            saveCheckpoint = true,
            fromDateInclusive = LocalDate.EPOCH.minusDays(2),
            untilDateExclusive = LocalDate.EPOCH
        )

        verify(exactly = 1) { agent.saveCheckpoint(listOf(LocalDate.EPOCH.minusDays(2), LocalDate.EPOCH.minusDays(1))) }
        verify(exactly = 1) { s3.put(any(), any(), """{"last-day-processed":"${LocalDate.EPOCH.minusDays(1)}"}""") }

        confirmVerified()
    }

    @Test
    fun testUniqueRecordId() {
        val recordId = metadataAgent.uniqueEvidenceRecordId(LocalDate.EPOCH)
        val uuid = recordId.substringAfter("19700101-metadata-")
        UUID.fromString(uuid)
    }

    @Test
    fun testValidRelationPredicate() {
        val relation = Input.Relation("", "doi", "subject")
        val relation2 = Input.Relation("", "url", "subject")
        val relation3 = Input.Relation("", "url", "other")

        Assertions.assertTrue(metadataAgent.validRelation(relation))
        Assertions.assertFalse(metadataAgent.validRelation(relation2))
        Assertions.assertFalse(metadataAgent.validRelation(relation3))
    }

    @Test
    fun testActionIdOf() {
        val expected = "cdaed9c608a3d6b8d7f80546ee1a936b9dd6af93"
        val actual = metadataAgent.actionIdOf("10.5555/12345678", "is-identical-to", "10.2307/j.ctv125jpk1.15")
        Assertions.assertEquals(expected, actual)
    }

    @Test
    fun testRestApiFilterCombinations() {

        val nullFilters = metadataAgent.allFilterCombinations(null)
        Assertions.assertEquals(nullFilters, emptyList<String>())

        val emptyFilters = metadataAgent.allFilterCombinations(emptyList())
        Assertions.assertEquals(emptyFilters, emptyList<String>())

        val singleFilter = metadataAgent.allFilterCombinations(listOf("has-relation"))
        Assertions.assertEquals(singleFilter, listOf("has-relation:1"))

        val allCombinations = listOf(
            "has-relation:1,has-references:1,has-orcid:1",
            "has-relation:1,has-references:1,has-orcid:0",
            "has-relation:1,has-orcid:1,has-references:0",
            "has-references:1,has-orcid:1,has-relation:0",
            "has-relation:1,has-references:0,has-orcid:0",
            "has-references:1,has-relation:0,has-orcid:0",
            "has-orcid:1,has-relation:0,has-references:0"
        )
        val multipleFilters = metadataAgent.allFilterCombinations(listOf("has-relation", "has-references", "has-orcid"))
        Assertions.assertEquals(allCombinations, multipleFilters)
    }

    @Test
    fun testProgressString() {
        val message = ProgressMessage(LocalDate.EPOCH, LocalDate.EPOCH, 100)
        var actual = metadataAgent.progressString(message, 0)
        Assertions.assertEquals(
            """#progress: {"from-date":"1970-01-01","until-date":"1970-01-01","total-results":100,"count-done":0,"percentage":0.0}""",
            actual
        )
        actual = metadataAgent.progressString(message, 100)
        Assertions.assertEquals(
            """#progress: {"from-date":"1970-01-01","until-date":"1970-01-01","total-results":100,"count-done":100,"percentage":100.0}""",
            actual
        )
    }

    @Test
    fun testTotalResultsCount() {
        val agent = spyk(metadataAgent)

        val json1 = jsonFileToPage("query-response-1.json")

        every { agent.progressString(any(), any()) } returns "progress..."
        every { agent.pageForUrl(any()) } returns json1

        val actual = agent.totalResultsCount(listOf(""), false, LocalDate.EPOCH, LocalDate.EPOCH)

        Assertions.assertEquals(1817, actual)
    }

    @Test
    fun testPageForUrlException() {
        val agent = spyk(metadataAgent)
        val request = mockk<Request>()
        every { request.responseString() } throws Exception()
        try {
            agent.pageForUrl(request)
        } catch (e: Exception) {
        }
        verify(exactly = 1) { agent.pageForUrl(request) }
    }

    @Test
    fun testPartitionRecordOverLimit(){

        val action = Output.Action(
            id = "123",
            occurredAt = LocalDateTime.now(),
            url = "https://crossref.org",
            relationTypeId = "references",
            lookupWorkTypeId = true,
            subj = Output.Subject("abc", "journal"),
            observations = (1..100).map { observation }
        )

        val record = Output.EvidenceRecord(
            id = "abc",
            sourceId = "crossref",
            jwt = "xyz",
            timeStamp = LocalDateTime.now(),
            sourceToken = "123",
            license = "licence",
            agent = Output.Agent("crossref", "1"),
            pages = listOf(Output.Page((1..100).map { action }))
        )


        val r = metadataAgent.partitionRecord(LocalDate.EPOCH, listOf(record))

        // The original record results in ~5mb and
        // this is expected to lead to 8 partitions
        Assertions.assertEquals(8, r.size)

        r.forEach{
            assert(it.second.toByteArray().size < KAFKA_MESSAGE_MAX_BYTES)
        }
    }

    @Test
    fun testPartitionRecordWithinLimits(){

        val action = Output.Action(
            id = "123",
            occurredAt = LocalDateTime.now(),
            url = "https://crossref.org",
            relationTypeId = "references",
            lookupWorkTypeId = true,
            subj = Output.Subject("abc", "journal"),
            observations = (1..10).map { observation }
        )

        val record = Output.EvidenceRecord(
            id = "abc",
            sourceId = "crossref",
            jwt = "xyz",
            timeStamp = LocalDateTime.now(),
            sourceToken = "123",
            license = "licence",
            agent = Output.Agent("crossref", "1"),
            pages = listOf(Output.Page((1..10).map { action }))
        )


        val r = metadataAgent.partitionRecord(LocalDate.EPOCH, listOf(record))

        Assertions.assertEquals(1, r.size)

        assert(r[0].second.toByteArray().size < KAFKA_MESSAGE_MAX_BYTES)
    }

    @Test
    fun testPartitionRecordImpossible(){

        val action = Output.Action(
            id = "123",
            occurredAt = LocalDateTime.now(),
            url = "https://crossref.org",
            relationTypeId = "references",
            lookupWorkTypeId = true,
            subj = Output.Subject("abc", "journal"),
            observations = (1..10000).map { observation }
        )

        val record = Output.EvidenceRecord(
            id = "abc",
            sourceId = "crossref",
            jwt = "xyz",
            timeStamp = LocalDateTime.now(),
            sourceToken = "123",
            license = "licence",
            agent = Output.Agent("crossref", "1"),
            pages = listOf(Output.Page((1..2).map { action }))
        )

        val r = metadataAgent.partitionRecord(LocalDate.EPOCH, listOf(record))

        assert(r.isEmpty())
    }

    @Test
    fun testPartitionRecordPartial(){

        val action1 = Output.Action(
            id = "123",
            occurredAt = LocalDateTime.now(),
            url = "https://crossref.org",
            relationTypeId = "references",
            lookupWorkTypeId = true,
            subj = Output.Subject("abc", "journal"),
            observations = (1..10000).map { observation }
        )

        val action2 = Output.Action(
            id = "123",
            occurredAt = LocalDateTime.now(),
            url = "https://crossref.org",
            relationTypeId = "references",
            lookupWorkTypeId = true,
            subj = Output.Subject("abc", "journal"),
            observations = (1..10).map { observation }
        )

        val record = Output.EvidenceRecord(
            id = "abc",
            sourceId = "crossref",
            jwt = "xyz",
            timeStamp = LocalDateTime.now(),
            sourceToken = "123",
            license = "licence",
            agent = Output.Agent("crossref", "1"),
            pages = listOf(Output.Page(listOf(action1, action2)))
        )

        val r = metadataAgent.partitionRecord(LocalDate.EPOCH, listOf(record))

        assert(jsonMapper.writeValueAsString(action1).toByteArray().size > KAFKA_MESSAGE_MAX_BYTES)

        Assertions.assertEquals(1, r.size)

        assert(r[0].second.toByteArray().size < KAFKA_MESSAGE_MAX_BYTES)
    }

}
