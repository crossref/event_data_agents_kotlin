import org.crossref.agents.common.JWTSigner
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class JWTSignerTest {
    @Test
    fun testSignCorrectSecret() {
        val signer = JWTSigner("TEST,TEST2")
        // The JWT token was encrypted using TEST
        val expected =
            "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJjcm9zc3JlZiJ9.r_Hl1NqV0kysHREA2modtNuGh2MzIpz7RHzYt8dSxFc"
        Assertions.assertEquals(expected, signer.sign("crossref"))
    }

    @Test
    fun testSignIncorrectSecret() {
        // The JWT token was encrypted using TEST not TEST2, but the signer uses the first out of many secrets
        val signer = JWTSigner("TEST2,TEST")
        val actual =
            "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJjcm9zc3JlZiJ9.r_Hl1NqV0kysHREA2modtNuGh2MzIpz7RHzYt8dSxFc"
        Assertions.assertNotEquals(signer.sign("crossref"), actual)
    }

    @Test
    fun testSignOnlyOneSecret() {
        // The signer should instantiate with only one secret as well
        val signer = JWTSigner("TEST")
        // The JWT token was encrypted using TEST
        val expected =
            "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJjcm9zc3JlZiJ9.r_Hl1NqV0kysHREA2modtNuGh2MzIpz7RHzYt8dSxFc"
        Assertions.assertEquals(expected, signer.sign("crossref"))
    }
}