import io.mockk.every
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import io.mockk.spyk
import org.crossref.agents.common.Resources
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.fail
import org.springframework.core.io.Resource
import org.springframework.core.io.ResourceLoader
import java.io.File
import java.io.InputStream

@ExtendWith(MockKExtension::class)
class ResourcesTest {

    private val resourceLoader = mockk<ResourceLoader>()
    private val resources = Resources(resourceLoader)
    private val mock = spyk(resources)
    private val resource = mockk<Resource>()

    @Test
    fun testGetVersionFromFile() {

        every { resourceLoader.getResource(any()) } returns resource
        every { resource.inputStream } returns "0.1.0-SNAPSHOT".byteInputStream()

        Assertions.assertEquals("0.1.0-SNAPSHOT", mock.getVersion())
    }

    @Test
    fun testGetVersionFromFileWithWhiteSpaceCharacters() {

        every { resourceLoader.getResource(any()) } returns resource

        every { resource.inputStream } returns "0.1.0-SNAPSHOT\n".byteInputStream()
        Assertions.assertEquals("0.1.0-SNAPSHOT", mock.getVersion())

        every { resource.inputStream } returns "\n0.1.0-SNAPSHOT\n".byteInputStream()
        Assertions.assertEquals("0.1.0-SNAPSHOT", mock.getVersion())

        every { resource.inputStream } returns "\n 0.1.0-SNAPSHOT \n".byteInputStream()
        Assertions.assertEquals("0.1.0-SNAPSHOT", mock.getVersion())
    }

    @Test
    fun testGetVersionFromEmptyFile() {

        every { resourceLoader.getResource(any()) } returns resource
        every { resource.inputStream } returns "".byteInputStream()

        try {
            mock.getVersion()
            fail("Exception not thrown despite empty or null version")
        } catch (e: Exception) {
            println("Exception correctly thrown with message: ${e.message}")
        }
    }
}