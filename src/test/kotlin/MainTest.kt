import io.mockk.*
import io.mockk.junit5.MockKExtension
import org.crossref.agents.Application
import org.crossref.agents.NoArgumentsException
import org.crossref.agents.UnknownCommandException
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.fail

@ExtendWith(MockKExtension::class)
class MainTest {
    private val app = Application("")

    @Test
    fun testEmptyArgs() {
        val args = emptyArray<String>()
        try {
            app.run(*args)
            fail("The app should have thrown a NoArgumentsException")
        } catch (e: Exception) {
            Assertions.assertEquals(NoArgumentsException().javaClass, e.javaClass)
        }
    }

    @Test
    fun testUnknownCommand() {
        val args = arrayOf("thisIsNotGoingToWork")

        try {
            app.run(*args)
            fail("The app should have thrown an UnknownCommandException")
        } catch (e: Exception) {
            Assertions.assertEquals(UnknownCommandException().javaClass, e.javaClass)
        }
    }

    @Test
    fun testMetadataAgent() {
        val mock = spyk(app)
        val args = arrayOf("metadata")

        justRun { mock.runMetadataAgent(any()) }
        excludeRecords {
            mock.run(any())
        }

        mock.run(*args)

        verify(exactly = 1) { mock.runMetadataAgent(*args) }

        confirmVerified()
    }

    @Test
    fun testVersion() {
        val mock = spyk(app)
        val args = arrayOf("version")

        justRun { mock.printVersion() }
        excludeRecords {
            mock.run(any())
        }

        mock.run(*args)

        verify(exactly = 1) { mock.printVersion() }

        confirmVerified()
    }
}