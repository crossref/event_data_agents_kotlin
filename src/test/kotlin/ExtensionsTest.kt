import org.crossref.agents.common.*
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class ExtensionsTest {

    @Test
    fun stringToSHA1() {
        val expected = "080612d0d521119326c00cf9907629b2a8e8b049"
        val actual =
            "https://doi.org/10.1515/9780822397014-013metadata-agent-relationsis-identical-to10.2307/j.ctv125jpk1.15".toSHA1()
        Assertions.assertEquals(expected, actual)
    }

    @Test
    fun longToMinutesString() {
        Assertions.assertEquals("2.5 minutes", 150_000L.toMinutesString())
        Assertions.assertEquals("2.0 minutes", 120_000L.toMinutesString())
        Assertions.assertEquals("1.5 minutes", 90_000L.toMinutesString())
        Assertions.assertEquals("1 minute", 60_000L.toMinutesString())
        Assertions.assertEquals("0.5 minutes", 30_000L.toMinutesString())
        Assertions.assertEquals("0 minutes", (-60_000L).toMinutesString())
    }

    @Test
    fun testNormaliseDOI() {
        mapOf(
            "https://doi.org/10.5555/12345678" to "10.5555/12345678",
            "https://doi.org/10.5555/12345678" to "doi:10.5555/12345678",
            "https://doi.org/10.5555/12345678" to "https://dx.doi.org/10.5555/12345678",
            "https://doi.org/10.5555/12345678" to "https://dx.doi.org/10.5555/12345678",
            "https://doi.org/10.5555/12345678" to "http://doi.org/10.5555/12345678",
            "https://doi.org/10.5555/12345678" to "http://dx.doi.org/10.5555/12345678"
        ).forEach {
            Assertions.assertEquals(it.key, it.value.toNormalisedDOI())
        }
    }

    @Test
    fun testToNonURLDOI() {
        mapOf(
            "10.5555/12345678" to "10.5555/12345678",
            "10.5555/12345678" to "dx.doi.org/10.5555/12345678",
            "10.5555/12345678" to "http://dx.doi.org/10.5555/12345678",
            "10.5555/12345678" to "https://dx.doi.org/10.5555/12345678",
            "10.5555/12345678" to "http://doi.org/10.5555/12345678",
            "10.5555/12345678" to "doi:10.5555/12345678"
        ).forEach {
            Assertions.assertEquals(it.key, it.value.toNonUrlDOI())
        }
    }

    @Test
    fun testPrefixWith() {
        Assertions.assertEquals(
            "https://doi.org/10.5555/12345678",
            "10.5555/12345678".prefixWith(DOI_DOT_ORG_URL)
        )
        Assertions.assertEquals(
            "https://doi.org/10.5555/12345678",
            "https://doi.org/10.5555/12345678".prefixWith(DOI_DOT_ORG_URL)
        )
    }
}