package org.crossref.agents

import io.sentry.Sentry
import org.crossref.agents.common.Resources
import org.crossref.agents.metadata.MetadataAgent
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.beans.factory.getBean
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.ApplicationContext
import kotlin.system.exitProcess

class NoArgumentsException : Exception()
class UnknownCommandException : Exception()

@SpringBootApplication
class Application(
    @Value("\${AGENT_METADATA_SENTRY_DSN:}") private val sentryDSN: String,
) : CommandLineRunner {

    @Autowired
    private lateinit var context: ApplicationContext

    init {
        Sentry.init { options ->
            options.dsn = sentryDSN
        }
    }

    override fun run(vararg args: String) {

        val logger: Logger = LoggerFactory.getLogger(Application::class.java)

        if (args.isEmpty()) {
            logger.error("A single command argument is required")
            throw NoArgumentsException()
        } else {
            when (args[0]) {
                "metadata" -> {
                    runMetadataAgent(*args)
                }

                "version" -> {
                    printVersion()
                }

                else -> {
                    logger.error("Unknown command ${args[0]}")
                    throw UnknownCommandException()
                }
            }
        }
    }

    fun runMetadataAgent(vararg args: String) {
        val metadataAgent: MetadataAgent = context.getBean(MetadataAgent::class) as MetadataAgent
        metadataAgent.run(args.drop(1).toTypedArray())
    }

    fun printVersion() {
        println((context.getBean(Resources::class) as Resources).getVersion())
    }
}

fun main(args: Array<String>) {
    try {
        SpringApplication.run(Application::class.java, *args)
    } catch (e: Exception) {
        e.printStackTrace()
        Sentry.captureException(e)
    } finally {
        exitProcess(1)
    }

}