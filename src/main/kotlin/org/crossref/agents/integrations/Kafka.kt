package org.crossref.agents.integrations

import io.sentry.Sentry
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.common.errors.RecordTooLargeException
import org.apache.kafka.common.serialization.StringSerializer
import org.springframework.beans.factory.annotation.Value
import org.springframework.kafka.core.DefaultKafkaProducerFactory
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.kafka.core.ProducerFactory
import org.springframework.retry.annotation.Backoff
import org.springframework.retry.annotation.Retryable
import org.springframework.stereotype.Service
import java.lang.Exception

const val KAFKA_MESSAGE_MAX_BYTES = 1048576

@Service
class Kafka(
    @Value("\${GLOBAL_KAFKA_BOOTSTRAP_SERVERS}") private val bootstrapServers: String
) {

    private val kafkaTemplate: KafkaTemplate<String, String> = KafkaTemplate(producerFactory())

    private fun producerFactory(): ProducerFactory<String, String> {
        val configProps: MutableMap<String, Any> = HashMap()
        configProps[ProducerConfig.BOOTSTRAP_SERVERS_CONFIG] = bootstrapServers
        configProps[ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG] = StringSerializer::class.java
        configProps[ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG] = StringSerializer::class.java
        return DefaultKafkaProducerFactory(configProps)
    }

    @Retryable( value = [Exception::class], maxAttempts = 3, backoff = Backoff(delay = 5000))
    fun send(topic: String, key: String, data: String){
        try {
            kafkaTemplate.send(topic, key, data).get()
        }catch (e: RecordTooLargeException){
            Sentry.captureException(e)
        }
    }
}