package org.crossref.agents.integrations

import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.model.GetObjectRequest
import com.amazonaws.services.s3.model.PutObjectResult
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service("AwsS3")
class AwsS3(
    @Value("\${AGENT_CHECKPOINT_S3_KEY:}") private val accessKey: String,
    @Value("\${AGENT_CHECKPOINT_S3_SECRET:}") private val secretKey: String,
    @Value("\${AGENT_CHECKPOINT_S3_REGION_NAME:eu-west-1}") private val region: String
) {

    private val s3Client: AmazonS3 = s3Client()

    private fun s3Client(): AmazonS3 =
        AmazonS3ClientBuilder.standard()
            .withCredentials(
                AWSStaticCredentialsProvider(
                    BasicAWSCredentials(accessKey, secretKey)
                )
            )
            .withRegion(Regions.fromName(region))
            .build()

    fun get(
        bucketName: String,
        targetFilename: String
    ): String =
        s3Client
            .getObject(GetObjectRequest(bucketName, targetFilename))
            .objectContent
            .use { inStream -> return inStream.bufferedReader().use { it.readText() } }

    fun put(
        bucketName: String,
        targetFilename: String,
        content: String
    ): PutObjectResult = s3Client.putObject(bucketName, targetFilename, content)

    fun fileExists(
        bucketName: String,
        targetFilename: String,
    ) = s3Client.doesObjectExist(bucketName, targetFilename)
}
