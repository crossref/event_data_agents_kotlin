package org.crossref.agents.metadata

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.util.StdDateFormat
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.readValue
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.fuel.core.extensions.httpString
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.google.common.util.concurrent.RateLimiter
import io.sentry.Sentry
import kotlinx.coroutines.*
import org.apache.commons.cli.CommandLine
import org.apache.commons.cli.CommandLineParser
import org.apache.commons.cli.DefaultParser
import org.apache.commons.cli.Options
import org.apache.commons.math3.util.CombinatoricsUtils
import org.crossref.agents.common.*
import org.crossref.agents.integrations.AwsS3
import org.crossref.agents.integrations.KAFKA_MESSAGE_MAX_BYTES
import org.crossref.agents.integrations.Kafka
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.retry.annotation.EnableRetry
import org.springframework.stereotype.Service
import java.io.File
import java.time.Duration
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger
import kotlin.io.path.Path
import kotlin.io.path.listDirectoryEntries
import kotlin.io.path.name
import kotlin.streams.asSequence
import kotlin.system.measureTimeMillis


const val SOURCE_TOKEN = "36c35e23-8757-4a9d-aacf-345e9b7eb50d"
const val LICENSE = "https://creativecommons.org/publicdomain/zero/1.0/"
const val SOURCE_ID = "crossref"
const val AGENT_NAME = "metadata-agent"
const val MAX_RETRIES = 3

private const val PLAINTEXT = "plaintext"
private const val REFERENCES = "references"
private const val CHECKPOINT_FILENAME = "checkpoint/metadata-agent-checkpoint.json"

val METADATA_AGENT_EPOCH: LocalDate = LocalDate.of(2021, 1, 1)
private val jsonMapper = ObjectMapper().apply {
    registerKotlinModule()
    registerModule(JavaTimeModule())
    disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
    dateFormat = StdDateFormat()
}

class MissingTypeException(message: String?) : Exception(message)
class RecordSizeException(message: String?) : Exception(message)

data class ProgressMessage(
    @JsonProperty("from-date") val fromDate: LocalDate,
    @JsonProperty("until-date") val untilDate: LocalDate,
    @JsonProperty("total-results") val totalResults: Int,
    @JsonProperty("count-done") var countDone: Int = 0,
    var percentage: Float = 0f
)

/**
 * The Crossref Metadata Agent. It retrieves works from the REST API and creates
 * events for each relationship.
 */
@Service("MetadataAgent")
@EnableRetry
class MetadataAgent(
    @Value("\${GLOBAL_REST_API_URI}") private val restApiUri: String,
    @Value("\${AGENT_CHECKPOINT_S3_BUCKET_NAME:}") private val s3CheckpointBucket: String,
    @Value("\${PERCOLATOR_INPUT_EVIDENCE_RECORD_TOPIC}") private val evidenceRecordsTopic: String,
    @Value("\${AGENT_METADATA_DAEMON_INTERVAL:600000}") private val daemonInterval: String,
    @Value("\${AGENT_METADATA_QUERY_ROWS:1000}") private val queryRows: Int,
    @Value("\${AGENT_METADATA_EMAIL:metadata-agent@crossref.org}") private val agentEmail: String,
    @Value("\${AGENT_METADATA_RATE_LIMIT:0}") private val agentRateLimit: Int,
    @Value("\${AGENT_METADATA_THREADS:4}") private val agentThreads: Int,
    @Value("\${AGENT_METADATA_CONNECTIONS:4}") private val agentConnections: Int,
    @Autowired var s3: AwsS3,
    @Autowired var kafka: Kafka,
    @Autowired var resources: Resources,
    @Autowired var jwtSigner: JWTSigner
) {

    private val options: Options = Options()
    private val logger: Logger = LoggerFactory.getLogger(MetadataAgent::class.java)
    private var enabledFunctions: List<Pair<(Input.Item) -> Sequence<Output.Action>, String>> = emptyList()

    // 2021-10-12 - Guava version: 30.1.1-jre
    // Any warnings related to the RateLimiter being annotated as @Beta
    // are related to this issue https://github.com/google/guava/issues/2797.
    // The RateLimiter is safe to use for our particular use case.
    private var requestsRateLimiter = RateLimiter.create(
        10.0,
        Duration.of(1, ChronoUnit.SECONDS)
    )

    val connectionsPool = Executors
        .newFixedThreadPool(agentConnections)
        .asCoroutineDispatcher()

    init {
        options.addOption(
            "d",
            false,
            "run in daemon mode, saveCheckpoint enforced, accepts fromDate, ignores untilDate"
        )
        options.addOption("f", true, "define fromDate, scope: inclusive, format: YYY-MM-DD")
        options.addOption("u", true, "define untilDate, scope: exclusive, format: YYY-MM-DD")
        options.addOption("s", true, "toggle saveCheckpoint (true/false)")
        options.addOption("p", false, "use the polite pool of the REST API")
        options.addOption("r", false, "Process relationships")
        options.addOption("e", false, "Process references")

        // Ensure longer time limits for our connection to the REST API
        FuelManager.instance.timeoutInMillisecond = 1 * 60 * 1000
        FuelManager.instance.timeoutReadInMillisecond = 1 * 60 * 1000

    }

    /**
     * Dispatcher function
     */
    fun run(args: Array<String> = emptyArray()) {
        val parser: CommandLineParser = DefaultParser()
        val cmd: CommandLine = parser.parse(options, args)
        val usePolitePool = cmd.hasOption("p")

        val availableFunctions = listOf(
            Pair(::getRelationsAsActions, "has-relation") to cmd.hasOption("r"),
            Pair(::getReferencesAsActions, "has-references") to cmd.hasOption("e")
        )

        enabledFunctions = if (availableFunctions.any { fnPair -> fnPair.second })
        // if there are any specific options use these
            availableFunctions
                .filter { fnPair -> fnPair.second }
                .map { fnPair -> fnPair.first }
        else
        // otherwise, include all available functions
            availableFunctions
                .map { fnPair -> fnPair.first }

        // Running in daemon mode ignores any further command line
        // options and enforces saving the checkpoint.
        if (cmd.hasOption("d")) {
            runDaemon(usePolitePool)
        } else {
            val saveCheckpoint = if (cmd.hasOption("s")) {
                cmd.getOptionValue("s").toBoolean()
            } else {
                null
            }

            if (cmd.hasOption("f")) {
                val fromDateInclusive: LocalDate? = cmd.getOptionValue("f")?.let { LocalDate.parse(it) }
                val untilDateExclusive: LocalDate? = cmd.getOptionValue("u")?.let { LocalDate.parse(it) }

                processPeriod(usePolitePool, saveCheckpoint ?: false, fromDateInclusive, untilDateExclusive)
            } else {
                processFromCheckpoint(usePolitePool, saveCheckpoint)
            }
        }
    }

    fun processPeriod(
        usePolitePool: Boolean,
        saveCheckpoint: Boolean,
        fromDateInclusive: LocalDate?,
        untilDateExclusive: LocalDate? = null
    ) {
        if (fromDateInclusive != null) {
            if (untilDateExclusive != null) {
                // As a default we won't save the checkpoint when a closed range of dates is provided
                // as that could be well in the past and may not reflect the work done past the
                // range we try to re-process.
                process(usePolitePool, saveCheckpoint, fromDateInclusive, untilDateExclusive)
            } else {
                process(usePolitePool, saveCheckpoint, fromDateInclusive)
            }
        }
    }

    fun processFromCheckpoint(usePolitePool: Boolean, saveCheckpoint: Boolean?): Boolean {
        val checkpoint = getExistingCheckpoint(s3CheckpointBucket)
        if (checkpoint != null) {
            logger.info("Last checkpoint: $checkpoint")

            // The last day we processed is already done, so we need to start from the next one
            checkpoint.lastDayProcessed.plusDays(1).let {
                // Is it before today?
                if (it.isBefore(LocalDate.now())) {
                    process(usePolitePool, saveCheckpoint ?: true, it)
                } else {
                    logger.info("The agent has already processed all available data up to $it")
                }
            }
        } else {
            logger.error("Checkpoint not found, aborting...")
        }
        return true
    }

    /**
     * Run in daemon mode. First run once from a specified date if provided.
     * Then run continuous according to saved checkpoint.
     * After each processContinuous sleep for daemonInterval (default 1 minute) and check again for any changes to the checkpoint
     * or if enough time has passed since last time we processed.
     */
    private fun runDaemon(usePolitePool: Boolean) {
        logger.info("Running in daemon mode...")
        val interval = daemonInterval.toLong().let { if (it < 0L) 60_000L else it }

        while (processFromCheckpoint(usePolitePool, true)) runBlocking {
            logger.info("Finished processing, sleeping for ${interval.toMinutesString()}")
            delay(interval)
        }
    }

    fun getExistingCheckpoint(s3Bucket: String): IO.CheckPoint? =
        if (s3.fileExists(s3Bucket, CHECKPOINT_FILENAME)) {
            val rsp = s3.get(s3Bucket, CHECKPOINT_FILENAME)
            val checkpoint: IO.CheckPoint = jsonMapper.readValue(rsp)
            checkpoint
        } else {
            null
        }

    fun pageForUrl(request: Request, retries: Int = 0): Input.Page {
        try {


            val (_, _, result) = runBlocking(connectionsPool) {
                async {
                    // Ensure we do not exceed the maximum rate limit.
                    requestsRateLimiter.acquire()
                    request.responseString()
                }.await()
            }

            return when (result) {
                is Result.Failure -> {
                    throw result.getException()
                }

                is Result.Success -> {
                    jsonMapper.readValue(result.get())
                }

                else -> throw IllegalStateException("Neither success or failure was reported for ${request.httpString()}")
            }
        } catch (e: Exception) {
            if (retries < MAX_RETRIES) {
                (retries + 1).let { retry ->
                    logger.info(
                        "Retrying $retry/$MAX_RETRIES: ${
                            request.httpString().trim()
                        } due to ${e.cause?.message}"
                    )
                    TimeUnit.SECONDS.sleep(retry.toLong())
                    return pageForUrl(request, retry)
                }
            } else {
                logger.error("Failed to complete request after $MAX_RETRIES attempts")
                throw e
            }
        }
    }

    /**
     * Given a list of binary filters return all possible AND filter combinations that would retrieve the same data as an
     * OR comparison of all the filters.
     *
     * Returns an empty list for a null or empty filters list.
     */
    fun allFilterCombinations(binaryFilters: List<String>?): List<String> {

        if (binaryFilters.isNullOrEmpty()) {
            return emptyList()
        } else if (binaryFilters.size == 1) {
            return listOf("${binaryFilters[0]}:1")
        }

        return (binaryFilters.indices).flatMap {
            CombinatoricsUtils.combinationsIterator(binaryFilters.size, binaryFilters.size - it).asSequence()
                .map { combo ->

                    val enabledFiltersNames = combo.map { efn -> binaryFilters[efn] }
                    val disabledFiltersNames = binaryFilters.filter { dfn -> !enabledFiltersNames.contains(dfn) }

                    val enabledFilters = enabledFiltersNames
                        .map { efn -> "$efn:1" }
                        .reduce { a, b -> "$a,$b" }

                    when (disabledFiltersNames.size) {
                        0 -> enabledFilters
                        1 -> "$enabledFilters,${disabledFiltersNames[0]}:0"
                        else -> {
                            val disabledFilters = disabledFiltersNames
                                .map { dfn -> "$dfn:0" }
                                .reduce { a, b -> "$a,$b" }
                            "$enabledFilters,$disabledFilters"
                        }
                    }
                }
        }
    }

    fun buildRequest(
        usePolitePool: Boolean,
        binaryFilters: String,
        fromDate: LocalDate,
        untilDate: LocalDate,
        cursor: String,
        countOnly: Boolean = false
    ): Request {
        val parameters = mutableListOf(
            "filter" to "$binaryFilters,from-update-date:$fromDate,until-update-date:$untilDate",
            "rows" to if (countOnly) 0 else queryRows,
        )

        if (!countOnly)
            parameters.add("cursor" to cursor)

        if (usePolitePool) {
            parameters.add(Pair("mailto", agentEmail))
        }
        return "${restApiUri}/works".httpGet(parameters)
    }

    /**
     * Get the sum of all results for all filters
     */
    fun totalResultsCount(
        binaryFilters: List<String>,
        usePolitePool: Boolean,
        fromDate: LocalDate,
        untilDate: LocalDate
    ): Int = allFilterCombinations(binaryFilters).sumOf { binaryFilter ->
        pageForUrl(
            buildRequest(
                usePolitePool,
                binaryFilter,
                fromDate, untilDate,
                "",
                true
            )
        ).message.totalResults
    }

    fun progressString(message: ProgressMessage, countDone: Int): String {
        countDone.also { message.countDone = it }
        ((countDone / message.totalResults.toFloat()) * 100f).also { message.percentage = it }
        return "#progress: ${jsonMapper.writeValueAsString(message)}"
    }

    fun collectDataForDates(
        usePolitePool: Boolean,
        fromDate: LocalDate,
        untilDate: LocalDate
    ): Sequence<Input.Page> = sequence {
        val logger: Logger = LoggerFactory.getLogger(MetadataAgent::class.java)
        val binaryFilters = enabledFunctions.map { fnPair -> fnPair.second }
        val countDone = AtomicInteger(0)

        logger.info("Calculating total results for $fromDate - $untilDate...")
        val count: Int = totalResultsCount(binaryFilters, usePolitePool, fromDate, untilDate)
        val progressMessage = ProgressMessage(fromDate, untilDate, count)

        logger.info(progressString(progressMessage, countDone.get()))

        allFilterCombinations(binaryFilters).forEach { binaryFilter ->
            var cursor = "*"
            do {
                val request = buildRequest(usePolitePool, binaryFilter, fromDate, untilDate, cursor)
                logger.info("Requesting: ${request.httpString().trim()}")
                val page = pageForUrl(request)

                // Grab the next cursor and num of returned items
                // Set to blank if none is available, this will terminate the loop
                cursor = page.message.nextCursor ?: ""

                // Send out the work
                yield(page)

                logger.info(progressString(progressMessage, countDone.addAndGet(page.message.items.size)))

            } while (page.message.items.size == queryRows && cursor.isNotBlank())
        }
    }


    fun uniqueEvidenceRecordId(date: LocalDate) =
        "${date.format(DateTimeFormatter.ofPattern("YYYYMMdd"))}-metadata-${UUID.randomUUID()}"

    fun logRecordSizeException(record: Output.EvidenceRecord, sizeBytes: Int) =
        record.pages[0].actions[0].id.let { actionId ->
            Sentry.captureException(RecordSizeException(actionId))
            logger.error("RecordSizeException - ActionId: $actionId")
            val inputContent = record.pages[0].actions[0].observations[0].inputContent.let { inputContent ->
                if (inputContent.length > 1024) {
                    inputContent.substring(0, 1024)
                } else {
                    inputContent
                }
            }
            logger.error(
                "Record size of $sizeBytes is greater than allowed size of $KAFKA_MESSAGE_MAX_BYTES. " +
                        "Subject Id: ${record.pages[0].actions[0].subj.pid} Observation input content: $inputContent"
            )
        }

    fun partitionRecord(date: LocalDate, records: List<Output.EvidenceRecord>): List<Pair<String, String>> =
        records.flatMap { record ->

            // Convert to json string to estimate final size
            val jsonStringValue = jsonMapper.writeValueAsString(record)
            val sizeBytes = jsonStringValue.toByteArray(Charsets.UTF_8).size

            if (sizeBytes >= KAFKA_MESSAGE_MAX_BYTES) {
                if (record.pages[0].actions.size == 1) {
                    // We cannot split further and the size is still greater than the limit.
                    // There is nothing else we can do at this point.
                    logRecordSizeException(record, sizeBytes)
                    emptyList()
                } else {
                    // Split in half and try again
                    record.pages[0].actions.let { actions ->
                        val firstHalf =
                            record.copy(
                                pages = listOf(Output.Page(actions.subList(0, actions.size / 2)))
                            )
                        val secondHalf = record.copy(
                            id = uniqueEvidenceRecordId(date),
                            pages = listOf(Output.Page(actions.subList(actions.size / 2, actions.size)))
                        )
                        // Recurse
                        partitionRecord(date, listOf(firstHalf, secondHalf))
                    }
                }
            } else {
                listOf(record.id to jsonStringValue)
            }
        }


    private fun Input.Page.toEvidenceRecords(date: LocalDate): Sequence<Pair<String, String>> = sequence {
        message.items
            .flatMap { item ->
                toActions(item)
            }.chunked(20)
            .forEach { actions ->
                val initialRecord = Output.EvidenceRecord(
                    id = uniqueEvidenceRecordId(date),
                    sourceId = SOURCE_ID,
                    jwt = jwtSigner.sign(SOURCE_ID),
                    timeStamp = timeStamp(),
                    sourceToken = SOURCE_TOKEN,
                    license = LICENSE,
                    agent = Output.Agent(AGENT_NAME, resources.getVersion()),
                    pages = listOf(Output.Page(actions))
                )
                partitionRecord(date, listOf(initialRecord)).forEach { yield(it) }
            }
    }

    fun timeStamp(value: LocalDateTime? = null): LocalDateTime = value ?: LocalDateTime.now()

    fun getRateLimit() =
        if (agentRateLimit > 0)
            agentRateLimit.toDouble()
        else
        // check the rate limits and go at 80% of the allowed throughput
            "${restApiUri}/works".httpGet().responseString().second.headers
                .let { headers ->
                    val limit = headers["x-ratelimit-limit"].first().toInt()
                    val interval = headers["x-ratelimit-interval"].first().dropLast(1).toInt()
                    (limit / interval) * 0.8
                }

    /**
     * This is the core function of the Metadata Agent
     */
    fun process(
        usePolitePool: Boolean,
        saveCheckpoint: Boolean,
        fromDateInclusive: LocalDate = METADATA_AGENT_EPOCH,
        untilDateExclusive: LocalDate = LocalDate.now()
    ) {

        requestsRateLimiter = getRateLimit().let {
            logger.info("Rate limit: $it/sec")
            // with a warmup period of 60s
            RateLimiter.create(it, Duration.of(60, ChronoUnit.SECONDS))
        }

        // Either use agentThreads or the num of days between from and until date, whichever is less
        val numThreads = ChronoUnit.DAYS.between(fromDateInclusive, untilDateExclusive).toInt().let {
            if (agentThreads > it) it else agentThreads
        }

        logger.info("Threads: $numThreads")
        logger.info("Concurrent connections: $agentConnections")

        // Fixed thread pool
        val dispatcher = Executors
            .newFixedThreadPool(numThreads)
            .asCoroutineDispatcher()

        // Temporary storage
        val localStoragePath = "evidence-records"

        fromDateInclusive.datesUntil(untilDateExclusive).asSequence()
            .chunked(numThreads)
            .forEach { chunkOfDays ->
                runBlocking {
                    chunkOfDays.map { day ->
                        launch(dispatcher) {
                            val tmp = "./$localStoragePath/$day"

                            // Delete temporary directory if it already exists
                            File(tmp).let {
                                if (it.exists()) {
                                    if (!it.deleteRecursively()) {
                                        throw Exception("Failed to delete pre-existing temporary directory $tmp")
                                    }
                                }
                            }

                            // Create the temporary directory
                            if (!File(tmp).mkdirs()) {
                                throw Exception("Failed to create temporary directory $tmp")
                            }

                            // Process and store locally
                            collectDataForDates(usePolitePool, day, day)
                                .filter { page -> page.status == "ok" }
                                .forEach { page ->
                                    var i = 0
                                    val duration = measureTimeMillis {
                                        page.toEvidenceRecords(day).forEach { (recordId, recordValue) ->
                                            // We want to write files to local storage until we have processed a day fully
                                            File("$tmp/$recordId").bufferedWriter().use { it.write(recordValue) }
                                            i++
                                        }
                                    }
                                    logger.info("Processed $i pending evidence records in: ${duration}ms (${duration / 1000.0f}s)")
                                }

                            // Push all locally stored evidence records
                            var i = 0
                            val duration = measureTimeMillis {
                                Path(tmp).listDirectoryEntries().forEach { record ->
                                    kafka.send(
                                        evidenceRecordsTopic,
                                        record.name,
                                        File(record.toUri()).bufferedReader().use { it.readText() }
                                    )
                                    i++
                                    if(i % 10_000 == 0){
                                        logger.info("Pushed $i evidence record...")
                                    }
                                }
                            }
                            logger.info("Pushed $i pending evidence records in: ${duration}ms (${duration / 1000.0f}s)")

                            // Delete after use
                            if (!File("$localStoragePath/$day").deleteRecursively()) {
                                throw Exception("Failed to delete temporary directory $tmp")
                            }
                        }
                        logger.info("Processing: $day")
                    }
                }
                // --- Save checkpoint
                if (saveCheckpoint) {
                    saveCheckpoint(chunkOfDays)
                }
            }
    }

    fun saveCheckpoint(chunkOfDays: List<LocalDate>) =
        chunkOfDays.maxByOrNull { day -> day.toEpochDay() }?.let {
            val checkpoint = IO.CheckPoint(it)
            s3.put(s3CheckpointBucket, CHECKPOINT_FILENAME, jsonMapper.writeValueAsString(checkpoint))
            logger.info("checkpoint: $checkpoint")
        }

    // --- Input data filtering
    val validRelation = { r: Input.Relation ->
        r.idType == "doi" &&
                r.assertedBy == "subject"
    }
    val validReference = { r: Input.Reference ->
        !r.doi.isNullOrBlank() || !r.unstructured.isNullOrBlank()
    }
    // ---

    fun actionIdOf(subjectDOI: String, relationType: String, relationId: String) =
        "${subjectDOI.toNormalisedDOI()}-metadata-agent-relations-${relationType}-${relationId}".uppercase()
            .toSHA1()

    /**
     * Apply a list of functions to the item and collect any actions.
     */
    fun toActions(item: Input.Item) =
        enabledFunctions.map { fnPair -> fnPair.first(item) }
            .filter { seq -> seq.any() }
            .fold(emptySequence<Output.Action>()) { acc, sequence -> acc + sequence }

    fun getRelationsAsActions(item: Input.Item): Sequence<Output.Action> =
        if (item.type.isNullOrEmpty()) {
            Sentry.captureException(MissingTypeException(item.doi))
            logger.error("Relations - item.type.isNullOrEmpty() - DOI: ${item.doi}")
            emptySequence()
        } else {
            sequence {
                item.relation
                    ?.filter { relation -> relation.value.isNotEmpty() }
                    ?.map { relation ->
                        relation.value
                            .filter(validRelation)
                            .map { r ->
                                yield(
                                    Output.Action(
                                        id = actionIdOf(item.doi, relation.key, r.id),
                                        url = item.doi.toNormalisedDOI(),
                                        occurredAt = item.indexed.dateTime,
                                        relationTypeId = relation.key,
                                        lookupWorkTypeId = true,
                                        subj = Output.Subject(item.doi.toNormalisedDOI(), item.type),
                                        observations = listOf(Output.Observation(PLAINTEXT, r.id))
                                    )
                                )
                            }
                    }
            }
        }

    fun getReferencesAsActions(item: Input.Item): Sequence<Output.Action> =
        if (item.type.isNullOrEmpty()) {
            Sentry.captureException(MissingTypeException(item.doi))
            logger.error("References - item.type.isNullOrEmpty() - DOI: ${item.doi}")
            emptySequence()
        } else {
            sequence {
                item.reference
                    .filter(validReference)
                    .map { reference ->
                        val observationValue = if (!reference.doi.isNullOrBlank()) {
                            reference.doi!!
                        } else {
                            reference.unstructured!!
                        }

                        yield(
                            Output.Action(
                                id = actionIdOf(item.doi, REFERENCES, observationValue),
                                url = item.doi.toNormalisedDOI(),
                                occurredAt = item.indexed.dateTime,
                                relationTypeId = REFERENCES,
                                lookupWorkTypeId = true,
                                subj = Output.Subject(item.doi.toNormalisedDOI(), item.type),
                                observations = listOf(Output.Observation(PLAINTEXT, observationValue))
                            )
                        )
                    }
            }
        }
}
