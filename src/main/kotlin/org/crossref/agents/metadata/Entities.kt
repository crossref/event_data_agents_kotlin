package org.crossref.agents.metadata

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDate
import java.time.LocalDateTime

class Input {

    @JsonIgnoreProperties(ignoreUnknown = true)
    data class Reference(
        @JsonProperty("DOI") var doi: String?,
        var unstructured: String?
    )

    @JsonIgnoreProperties(ignoreUnknown = true)
    data class Relation(
        var id: String,
        @JsonProperty("id-type") var idType: String,
        @JsonProperty("asserted-by") var assertedBy: String
    )

    @JsonIgnoreProperties(ignoreUnknown = true)
    data class Indexed(
        @JsonProperty("date-time") var dateTime: LocalDateTime
    )

    @JsonIgnoreProperties(ignoreUnknown = true)
    data class Item(
        val type: String?,
        val indexed: Indexed,
        @JsonProperty("DOI") var doi: String,
        var reference: List<Reference> = emptyList(),
        var relation: Map<String, List<Relation>>?
    )

    @JsonIgnoreProperties(ignoreUnknown = true)
    data class Message(
        var items: List<Item> = emptyList(),
        @JsonProperty("next-cursor") var nextCursor: String?,
        @JsonProperty("total-results") val totalResults: Int
    )

    @JsonIgnoreProperties(ignoreUnknown = true)
    data class Page(
        var status: String,
        var message: Message
    )
}

class Output {
    data class Agent(val name: String, val version: String)
    data class Page(val actions: List<Action> = emptyList())
    data class Subject(val pid: String, @JsonProperty("work_type_id") val workTypeId: String)
    data class Observation(val type: String, @JsonProperty("input-content") val inputContent: String)

    data class Action(
        val id: String,
        val url: String,
        @JsonProperty("occurred-at")
        @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        val occurredAt: LocalDateTime,
        @JsonProperty("relation-type-id") val relationTypeId: String,
        @JsonProperty("lookup-work-type-id") val lookupWorkTypeId: Boolean,
        val subj: Subject,
        val observations: List<Observation> = emptyList()
    )

    data class EvidenceRecord(
        val id: String,
        @JsonProperty("source-id") val sourceId: String,
        @JsonProperty("jwt") val jwt: String,
        @JsonProperty("timestamp")
        @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        val timeStamp: LocalDateTime,
        @JsonProperty("source-token") val sourceToken: String,
        val license: String,
        val agent: Agent,
        var pages: List<Page> = emptyList()
    )
}

class IO {
    data class CheckPoint(
        @JsonProperty("last-day-processed") val lastDayProcessed: LocalDate
    )
}