package org.crossref.agents.common

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
class JWTSigner(
    @Value("\${GLOBAL_JWT_SECRETS}") var csvSecrets: String
) {
    private val secrets: List<String> = csvSecrets.split(",")
    private val algorithm: Algorithm = Algorithm.HMAC256(secrets[0])

    fun sign(sourceId: String): String {
        return JWT.create()
            .withClaim("sub", sourceId)
            .sign(algorithm)
    }
}