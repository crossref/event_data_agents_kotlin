package org.crossref.agents.common

import org.apache.commons.io.IOUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.ResourceLoader
import org.springframework.stereotype.Component
import java.nio.charset.StandardCharsets

@Component
class Resources(
    @Autowired private val resourceLoader: ResourceLoader,
) {

    private var version: String? = null

    fun getVersion(): String {

        if (version == null) {
            version = IOUtils.toString(resourceLoader.getResource("classpath:VERSION.txt")
                .inputStream, StandardCharsets.UTF_8.name()).trim()
        }

        return if (!version.isNullOrBlank()) {
            version!!
        } else {
            throw Exception("Null or empty version file packaged with the executable")
        }
    }
}