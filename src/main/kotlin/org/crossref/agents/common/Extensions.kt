package org.crossref.agents.common

import org.apache.commons.codec.digest.DigestUtils

const val DOI_DOT_ORG_URL = "https://doi.org/"

/**
 * Returns this String prefixed with the prefix if and only if this String
 * is not already prefixed with the prefix. Otherwise, it returns this String.
 */
fun String.prefixWith(prefix: String) = if (this.startsWith(prefix)) {
    this
} else {
    prefix.plus(this)
}

fun String.toNormalisedDOI() = this.toNonUrlDOI().prefixWith(DOI_DOT_ORG_URL)

fun String.toNonUrlDOI(): String {
    listOf("http://", "https://", "dx.doi.org/", "doi.org/", "doi:").let { prefixes ->
        return prefixes.fold(this) { doi, prefix ->
            if (doi.startsWith(prefix)) {
                doi.substringAfter(prefix)
            } else {
                doi
            }
        }
    }
}

fun String.toSHA1() =
    DigestUtils.sha1Hex(this.toByteArray())!!

fun Long.toMinutesString() =
    when {
        this == 60_000L -> {
            "1 minute"
        }
        this <= 0L -> {
            "0 minutes"
        }
        else -> {
            "${this / 60_000.00} minutes"
        }
    }