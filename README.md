# Event Data Agents in Kotlin

This project aims to form the base for a set of event data agents developed in Kotlin.
The project uses Spring Boot a few Kotlin specific libraries as well as a tiny bit of Java libraries.
The agents are expected to be developed under their own package within `org.crossref.agents`.
Code that support's communication with external services should be placed under `org.crossref.agents.integrations` and for now shared code is placed directly on the top level along `main`. 

The first and only agent for the time being is the Crossref metadata agent. 

## Building and testing

To run the tests locally: ```mvn clean test```

A Gitlab CI file is included that takes care of automatically versioning, building and publishing the project's docker image.

## The agents 

### Crossref Metadata Agent

An agent that queries Cayenne's works endpoint and produces evidence records for each DOI relationship retrieved.

#### Environment variables

The following environment variables are mandatory except if stated otherwise:

- GLOBAL_REST_API_URI
- GLOBAL_KAFKA_BOOTSTRAP_SERVERS
- PERCOLATOR_INPUT_EVIDENCE_RECORD_TOPIC
- AGENT_METADATA_SENTRY_DSN: Optional, enables Sentry logging

By default, the agent saves a json file on S3 with the latest date processed.
The following variables are mandatory except if `saveState` is set to false.

- AGENT_CHECKPOINT_S3_BUCKET_NAME
- AGENT_CHECKPOINT_S3_KEY
- AGENT_CHECKPOINT_S3_REGION_NAME
- AGENT_CHECKPOINT_S3_SECRET

#### CLI Arguments

|arg|name|scope|format|meaning|
|---|---|---|---|---|
|f|fromDate|inclusive|YYY-MM-DD|collect data from and including this date|
|u|untilDate|exclusive|YYY-MM-DD|collect data up until but not including this date|
|s|saveState| |true/false|save the latest date that has been successfully processed|
|d|daemon| | |run as a daemon|
|p|polite| | |use the polite pool of the REST API|
|r|relationships| | |process relationships|
|e|references| | |process references|

#### Evidence Record Id

```YYYMMdd-metadata-UUID```

#### Action Id

```kotlin
"${subjectDOI.toNormalisedDOI()}-metadata-agent-relations-${relationType}-${relationId}".toUpperCase().toSHA1()
```

Example: `HTTPS://DOI.ORG/10.5555/12345678-METADATA-AGENT-RELATIONS-IS-IDENTICAL-TO-HTTPS://DOI.ORG/10.2307/J.CTV125JPK1.15` => `cdaed9c608a3d6b8d7f80546ee1a936b9dd6af93`