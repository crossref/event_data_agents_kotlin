#! /usr/bin/env bash

export GLOBAL_REST_API_URI=https://api.crossref.org/v1
export GLOBAL_KAFKA_BOOTSTRAP_SERVERS=localhost:9092
export PERCOLATOR_INPUT_EVIDENCE_RECORD_TOPIC=evidence-records
export AGENT_METADATA_SENTRY_DSN=""

java -jar target/agents*.jar "$@" -s false

